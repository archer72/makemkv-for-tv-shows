#!/bin/bash
echo "Which Series is this?"
read series
echo "Series: $series"                                     echo "What Season is this?"
read season                                                echo "Season: $season"
echo "Which disc # is this?"
read disknum                                               echo "This is disk #$disknum"
echo "Starting with which episode?"                        read episode                                               
eject -x20
mkdir ""disc."$disknum"                                    makemkvcon mkv --progress=-same --minlength=2100 disc:0 all ""disc."$disknum"
cd ""disc."$disknum"                                                                                                  #episode=1                                                 episode=$episode
for track in *.mkv
do
        mv $track $series"_S"$season"_D"$disknum"_E"$episode"_".mkv
        episode=$((episode+1))
done

cd ..
